package com.mitocode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Informacion del Prodcuto")
@Entity
@Table(name = "producto")
public class Producto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idProducto;
	
	@ApiModelProperty(notes = "nombre debe tener minimo 3 caracteres")
	@Size(min = 3 , message = "nombre debe tener minimo 3 caracteres")
	@Column(name = "nombre", length = 15, nullable = false)
	private String nombre;
	
	@ApiModelProperty(notes = "marca debe tener minimo 3 caracteres")
	@Size(min = 3 , message = "marca debe tener minimo 3 caracteres")
	@Column(name = "marca", length = 10, nullable = false)
	private String marca;

	public Integer getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

}
