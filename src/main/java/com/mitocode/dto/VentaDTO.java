package com.mitocode.dto;

import com.mitocode.model.Venta;

public class VentaDTO {
	
	private Venta venta;

	public Venta getVenta() {
		return venta;
	}

	public void setVenta(Venta venta) {
		this.venta = venta;
	}
	
	

}
