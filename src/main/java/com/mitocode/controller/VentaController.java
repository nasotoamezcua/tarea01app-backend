package com.mitocode.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.dto.VentaDTO;
import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Venta;
import com.mitocode.repo.service.IVentaService;

@RestController
@RequestMapping("ventas")
public class VentaController {
	
	@Autowired
	private IVentaService service;
	
	@GetMapping
	public ResponseEntity<List<Venta>> listar(){
		List<Venta> ventas = service.listar();
		
		if(ventas == null || ventas.isEmpty()) {
			 throw new ModeloNotFoundException("NO EXISTEN VENTAS");
		}
		
		return new ResponseEntity<List<Venta>>(ventas,HttpStatus.OK);
	}
	
	
	@GetMapping("/{id}")
	public ResponseEntity<Venta> listarPorId(@PathVariable("id") Integer id){
		Venta venta = service.listarPorId(id);
		if(venta.getIdVenta() == null) {
			throw new ModeloNotFoundException("ID NO SE ENCONTRO: " + id);
		}
		return new ResponseEntity<Venta>(venta, HttpStatus.OK);
	}

	
	/**
	 * Hateoas 1.0 > Spring Boot 2.1
	 * @param id
	 * @return
	 */
	@GetMapping("/hateoas/{id}")
	public Resource<Venta> listarPorIdHateoas(@PathVariable("id") Integer id){
		Venta venta = service.listarPorId(id);
		if(venta.getIdVenta() == null) {
			throw new ModeloNotFoundException("ID NO SE ENCONTRO: " + id);
		}
		
		Resource<Venta> recurso = new Resource<Venta>(venta);
		ControllerLinkBuilder linTo = linkTo(methodOn(this.getClass()).listarPorId(id));
		recurso.add(linTo.withRel("venta-resource"));
		return recurso;
	}
	
	
	@PostMapping
	public ResponseEntity<Object> registrar(@Valid @RequestBody Venta objeto){
		Venta venta = service.registrar(objeto);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(venta.getIdVenta()).toUri();
		
		return ResponseEntity.created(location).build();
	}
	
	/**
	 * Utilizando DTO
	 * @param dto
	 * @return
	 */
	@PostMapping("/dto")
	public ResponseEntity<Object> registrarDTO(@Valid @RequestBody VentaDTO dto){
		Venta venta = service.registarVentaDTO(dto);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(venta.getIdVenta()).toUri();
		
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<Venta> modificar(@Valid @RequestBody Venta objeto){
		Venta venta = service.modificar(objeto);
		if(venta == null ) {
			throw new ModeloNotFoundException("LA VENTA NO FUE ACTUALIZADA");
		}
		return new ResponseEntity<Venta>(venta, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id){
		service.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
	}

}
