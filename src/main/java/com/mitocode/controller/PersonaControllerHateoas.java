package com.mitocode.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Persona;
import com.mitocode.repo.service.IPersonaService;
import com.mitocode.util.Constantes;

@RestController
@RequestMapping("personas/hateoas")
public class PersonaControllerHateoas {
	
	@Autowired
	private IPersonaService service;
	
	@GetMapping
	public Resources<Resource<Persona>> listar(){
		
		 List<Persona> personas = service.listar();
		 
		 if(personas == null || personas.isEmpty()) {
			 throw new ModeloNotFoundException("NO EXISTEN PERSONAS");
		 }
		  
	      return createPersonasResources(personas);
	}
	
	
	@GetMapping("/{id}")
	public Resource<Persona> listarPorId(@PathVariable("id") Integer id){
		Persona persona = service.listarPorId(id);
		
		if(persona.getIdPersona() == null) {
			throw new ModeloNotFoundException("ID NO SE ENCONTRO: " + id);
		}
		
		Resource<Persona> resource = createPersonaResource(persona);
		ControllerLinkBuilder link = createLinkPersonas();
		resource.add(link.withRel(Constantes.WITHREL_PERSONAS));
		
		return resource;
	}	
	
	@PostMapping
	public ResponseEntity<Object> registrar(@Valid @RequestBody Persona objeto){
		Persona persona = service.registrar(objeto);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(persona.getIdPersona()).toUri();
		
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public Resource<Persona> modificar(@Valid @RequestBody Persona objeto){
		Persona persona = service.modificar(objeto);
		
		if(persona == null ) {
			throw new ModeloNotFoundException("LA PERSONA NO FUE ACTUALIZADA");
		}
		
		Resource<Persona> resource = createPersonaResource(persona);
		ControllerLinkBuilder link = createLinkPersonas();
		resource.add(link.withRel(Constantes.WITHREL_PERSONAS));
		
		return resource;
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id){
		service.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
	}
	
	/**
	 * Metodos de apoyo para generar los Links
	 * Utilizando Hateoas 1.0 > Spring Boot 2.1
	 * @param e
	 * @return
	 */
	private static Resource<Persona> createPersonaResource(Persona p) {
		Resource<Persona>  resource = new Resource<>(p);
		//Link linkTo = linkTo(methodOn(PersonaController.class).listarPorId(p.getIdPersona())).withSelfRel();
	    ControllerLinkBuilder linkTo = createLinkPersona(p);
	    resource.add(linkTo.withRel(Constantes.WITHREL_PERSONA));
	    return resource;
	}
	
	private static Resources<Resource<Persona>> createPersonasResources(List<Persona> personas) {
		
		List<Resource<Persona>> listResources = new ArrayList<>();
		
		/*Utilizando Stream puede pegar en el rendimiento de la aplicacion
	     List<Resource<Persona>> personaResources = personaList.stream()
	     							.map(PersonaController::createPersonaResource)
	     							.collect(Collectors.toList());
	     							
	     Con landas:
	      List<Resource<Persona>> personaResources = personaList.stream()
	      							.map(p -> createPersonasResources(p))
	      							.collect(Collectors.toList());
	      */
		
		personas.forEach(persona -> {
			listResources.add(createPersonaResource(persona));
		});
		
		Resources<Resource<Persona>> resources = new Resources<Resource<Persona>>(listResources);
		
		//Link linkTo = linkTo(methodOn(ProductoController.class).listar()).withSelfRel();
		ControllerLinkBuilder linkTo = createLinkPersonas();
	    resources.add(linkTo.withRel(Constantes.WITHREL_PERSONAS));
	    return resources;
	}
	
	private static ControllerLinkBuilder createLinkPersonas() {
		return linkTo(methodOn(PersonaControllerHateoas.class).listar());
	}
	
	private static ControllerLinkBuilder createLinkPersona(Persona p) {
		return linkTo(methodOn(PersonaControllerHateoas.class).listarPorId(p.getIdPersona()));
	}

}
