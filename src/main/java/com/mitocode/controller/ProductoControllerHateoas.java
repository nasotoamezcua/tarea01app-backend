package com.mitocode.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Producto;
import com.mitocode.repo.service.IProductoService;

@RestController
@RequestMapping("productos/hateoas")
public class ProductoControllerHateoas {
	
	@Autowired
	private IProductoService service;
	
	@GetMapping
	public Resources<Resource<Producto>> listar(){
		List<Producto> productos = service.listar();
		
		if(productos == null || productos.isEmpty()) {
			throw new ModeloNotFoundException("NO EXISTEN PRODUCTOS");
		}
		
		return createProductosResources(productos);
	}
	
	
	@GetMapping("/{id}")
	public Resource<Producto> listarPorId(@PathVariable("id") Integer id){
		Producto producto = service.listarPorId(id);
		
		if(producto.getIdProducto() == null) {
			throw new ModeloNotFoundException("ID NO SE ENCONTRO: " + id);
		}
		
		return createProductoResource(producto);
	}
	
	@PostMapping
	public ResponseEntity<Object> registrar(@Valid @RequestBody Producto objeto){
		Producto producto = service.registrar(objeto);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(producto.getIdProducto()).toUri();
		
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public Resource<Producto> modificar(@Valid @RequestBody Producto objeto){
		Producto producto = service.modificar(objeto);
		
		if(producto == null) {
			throw new ModeloNotFoundException("EL PRODUCTO NO FUE ACTUALIZADO");
		}
		
		return createProductoResource(producto);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id){
		service.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
	}
	
	/**
	 * Metodos de apoyo para generar los Links
	 * Utilizando Hateoas 1.0 > Spring Boot 2.1
	 * @param e
	 * @return
	 */
	private static Resource<Producto> createProductoResource(Producto p) {
		Resource<Producto>  resource = new Resource<>(p);
		//Link linkTo = linkTo(methodOn(ProductoController.class).listarPorId(p.getIdPersona())).withSelfRel();
	    ControllerLinkBuilder linkTo = linkTo(methodOn(ProductoControllerHateoas.class).listarPorId(p.getIdProducto()));
	    resource.add(linkTo.withRel("producto-resource"));
	    return resource;
	}
	
	private static Resources<Resource<Producto>> createProductosResources(List<Producto> productos) {
		
		List<Resource<Producto>> listResources = new ArrayList<>();
		
		productos.forEach(producto -> {
			listResources.add(createProductoResource(producto));
		});
		
		Resources<Resource<Producto>> resources = new Resources<Resource<Producto>>(listResources);
		//Link linkTo = linkTo(methodOn(ProductoController.class).listar()).withSelfRel();
		ControllerLinkBuilder linkTo = linkTo(methodOn(ProductoControllerHateoas.class).listar());
	    resources.add(linkTo.withRel("productos-resources"));
	    return resources;
	}

}
