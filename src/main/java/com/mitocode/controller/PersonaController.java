package com.mitocode.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Persona;
import com.mitocode.repo.service.IPersonaService;

@RestController
@RequestMapping("personas")
public class PersonaController {
	
	@Autowired
	private IPersonaService service;
	
	@GetMapping
	public ResponseEntity<List<Persona>> listar(){
		
		 List<Persona> personas = service.listar();
		 
		 if(personas == null || personas.isEmpty()) {
			 throw new ModeloNotFoundException("NO EXISTEN PERSONAS");
		 }
		  
	      return new ResponseEntity<List<Persona>>(personas, HttpStatus.OK);
	}
	
	
	@GetMapping("/{id}")
	public ResponseEntity<Persona> listarPorId(@PathVariable("id") Integer id){
		Persona persona = service.listarPorId(id);
		
		if(persona.getIdPersona() == null) {
			throw new ModeloNotFoundException("ID NO SE ENCONTRO: " + id);
		}
		
		return new ResponseEntity<Persona>(persona, HttpStatus.OK);
	}	
	
	@PostMapping
	public ResponseEntity<Object> registrar(@Valid @RequestBody Persona objeto){
		Persona persona = service.registrar(objeto);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(persona.getIdPersona()).toUri();
		
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<Persona> modificar(@Valid @RequestBody Persona objeto){
		Persona persona = service.modificar(objeto);
		
		if(persona == null ) {
			throw new ModeloNotFoundException("LA PERSONA NO FUE ACTUALIZADA");
		}
		
		return new ResponseEntity<Persona>(persona, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id){
		service.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
	}
}
