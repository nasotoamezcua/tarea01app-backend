package com.mitocode.exception;

import java.time.LocalDateTime;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class ResponseExceptionHandler extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ResponseException> manejarTodasException(ModeloNotFoundException ex, WebRequest request){
		ResponseException rsex = new ResponseException(LocalDateTime.now(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<ResponseException>(rsex, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(ModeloNotFoundException.class)
	public final ResponseEntity<ResponseException> manejarModeloException(ModeloNotFoundException ex, WebRequest request){
		ResponseException rsex = new ResponseException(LocalDateTime.now(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<ResponseException>(rsex, HttpStatus.NOT_FOUND);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ResponseException rsex = new ResponseException(LocalDateTime.now(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<Object>(rsex, HttpStatus.BAD_REQUEST);
	}

}
