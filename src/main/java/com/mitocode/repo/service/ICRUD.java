package com.mitocode.repo.service;

import java.util.List;

public interface ICRUD<T, ID> {
	
	T registrar(T obj);
	
	T modificar(T obj);
	
	List<T> listar();
	
	T listarPorId(ID id);
	
	boolean eliminar(ID id);

}
