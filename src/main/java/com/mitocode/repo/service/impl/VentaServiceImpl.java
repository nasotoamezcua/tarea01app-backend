package com.mitocode.repo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dto.VentaDTO;
import com.mitocode.model.Venta;
import com.mitocode.repo.IVentaRepo;
import com.mitocode.repo.service.IVentaService;

@Service
public class VentaServiceImpl implements IVentaService {
	
	@Autowired
	private IVentaRepo repo;
	

	@Override
	public Venta registrar(Venta obj) {
		obj.getDetalleVenta().forEach(det ->{
			det.setVenta(obj);
		});
		
		return repo.save(obj);
	}
	
	@Override
	public Venta registarVentaDTO(VentaDTO dto) {
		dto.getVenta().getDetalleVenta().forEach(det ->{
			det.setVenta(dto.getVenta());
		});
		
		return repo.save(dto.getVenta());
	}

	@Override
	public Venta modificar(Venta obj) {
		return repo.save(obj);
	}

	@Override
	public List<Venta> listar() {
		return repo.findAll();
	}

	@Override
	public Venta listarPorId(Integer id) {
		Optional<Venta> vt = repo.findById(id);
		return vt.isPresent() ? vt.get(): new Venta();
	}

	@Override
	public boolean eliminar(Integer id) {
		repo.deleteById(id);
		return true;
	}

	

}
